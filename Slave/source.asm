    include "ramdat.asm"
		      dc.l $FFFE00,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA GENESIS    "
              dc.b "(C) 2015 James L"
              dc.b "Still Alive slave program                       "
              dc.b "Still Alive slave program                       "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
        bsr setup_vdp
		bsr clear_vram
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$0800,d4
		bsr vram_loop
		
		move.l #$c0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$02ac,(a4)
		move.l #$00000000,d2
	    move.b #$02,artnum
	    move.b #$01,palette_number
	    bsr update_art
        move.w #$2300, sr       ;enable ints		
		
loop:
		bra loop

write_art:
		move.w #$0027,d4
artloop:
		move.b (a5)+,d5	
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,artloop
		move.w #$0017,d4
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		dbf d6, write_art
		rts
		

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts	

calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
update_art:	
		lea (art_table),a0
		move.l #$00000000,d0
		move.b artnum,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)		
		
art_table:		
 dc.w ld_art_aperture  ;artnum1
 dc.w ld_art_explosion ;artnum2
 dc.w ld_art_unknown1  ;artnum3
 dc.w ld_art_cake      ;artnum4
 dc.w ld_art_mesa      ;artnum5
 dc.w ld_art_atom      ;artnum6
 dc.w ld_art_fire      ;artnum7
 dc.w ld_art_heart     ;artnum8
 dc.w ld_art_nuke      ;artnum9
 dc.w ld_art_check     ;artnumA
 dc.w ld_art_empty     ;artnumB 
 dc.w ld_art_empty     ;artnumC 
 dc.w ld_art_empty     ;artnumD 
 dc.w ld_art_empty     ;artnumE 
 dc.w ld_art_empty     ;artnumF 
			
ld_art_aperture:
		lea (ascii_aperture),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_explosion:
		lea (ascii_explosion),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts		
ld_art_unknown1:
		lea (ascii_unknown1),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_cake:
		lea (ascii_cake),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_mesa:
		lea (ascii_mesa),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_atom:
		lea (ascii_atom),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art
		rts	
ld_art_fire:
		lea (ascii_fire),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_heart:
		lea (ascii_heart),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_nuke:
		lea (ascii_nuke),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_empty:
		lea (ascii_clear),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_check:
		lea (ascii_check),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts		
		
update_palette:
		cmpi.b #$00, palette_number
		 beq return
		lea (palette_table),a0
		move.l #$00000000,d0
		move.b palette_number,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)
		
palette_table:		
 dc.w ld_palette_1	;amber
 dc.w ld_palette_2	;white
 dc.w ld_palette_3	;green
	
ld_palette_1:	
		move.l #$c0060000,(a3)
		move.w #$02ac,(a4) ;amber
		rts
ld_palette_2:
		move.l #$c0060000,(a3)	
		move.w #$0eee,(a4) ;white
		rts
ld_palette_3:		
		move.l #$c0060000,(a3)
		move.w #$06e6,(a4) ;green
		rts
		
ErrorTrap:        
        bra ErrorTrap

HBlank:
	   add.w #$0001,hblanks
	   move.l #$50000010,(a3) ;write to VSRAM
	   move.w hblanks,d0 
	   muls.w #$FFDC,d0	   
	   lsr #$07,d0
	   move.w d0,(a4)
	   move.w d0,(a4)	
       rte

VBlank:
	   move.w #$0000,hblanks
	   bsr read_controller
	   
	   move.b d3,d2
	   andi.b #$f0,d2
	   lsr.b #$06,d2
	   move.b d2, palette_number
	   bsr update_palette
	   
	   move.b d3,d2	   
	   andi.b #$0f,d2
	   cmpi.b #$00,d2
	    beq returnint
	   cmp.b artnum,d2
	    beq returnint    ;skip art update if there isn't anything new
	   move.b d2,artnum
	   bsr update_art	   
       rte
	   
returnint:
	   rte
return:
	   rts	   
   
	include "data.asm"

ROM_End:
              
              