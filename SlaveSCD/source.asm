    include "ramdat.asm"

start:	
		bsr clear_regs
		move.w #$2700, sr       ;disable ints
						
		lea vblank(pc),a0
		move.l a0,$fffd08
		
		;lea hblank(pc),a1
		;move.l a1,$fffd0e
		;move.w #$FD0C,$A12006
		
		move.l #$00000000,a0
		move.l #$00000000,d0
		move.l #$00000000,a1
				
		
        bsr setup_vdp		
		bsr clear_vram			

		move.l #$40000000,(a3)
		lea (font,PC),a5
		move.w #$0800,d4
		bsr vram_loop

		move.l #$c0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$02ac,(a4)
		move.l #$00000000,d2

        move.w #$2300, sr       ;enable ints		
		
loop:
		move.w $C00008,d6
		cmp d6,d0
		bne hblank		
		bra loop

HBlank:
	   move.w $C00008,d1	  ;set new state
	   add.w #$0001,hblanks
	   move.l #$50000010,(a3) ;write to VSRAM
	   move.w hblanks,d0    
	   ;muls.w #$FFF4,d0	   
	   muls.w #$000b,d0
	   eor.w #$ffff,d0
	   lsr #$07,d0
	   move.w d0,(a4)
	   move.w d0,(a4)	
       ;rte
	   bra loop

VBlank:
	    move.w #$0000,hblanks
	    bsr read_controller
		
	    move.b d3,d2
	    andi.b #$f0,d2
	    lsr.b #$06,d2
	    move.b d2, palette_number
	    bsr update_palette

	   move.b d3,d2	   
	   andi.b #$0f,d2
	   cmpi.b #$00,d2
	    beq returnint
	   cmp.b artnum,d2
	    beq returnint    ;skip art update if there isn't anything new
	   move.b d2,artnum
	   bsr update_art		
        rte
	
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 	

clear_regs:
	   move.l #$00000000,d0
	   move.l #$00000000,d1
	   move.l #$00000000,d2
	   move.l #$00000000,d3
	   move.l #$00000000,d4
	   move.l #$00000000,d5
	   move.l #$00000000,d6
	   move.l #$00000000,d7
	   move.l #$00000000,a0
	   move.l #$00000000,a2
	   move.l #$00000000,a3
	   move.l #$00000000,a4
	   move.l #$00000000,a5
	   move.l #$00000000,a6
	   rts
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts	
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts		   
setup_vdp:
        move.l #$C00004,a3     ;VDP control port
	    move.l #$C00000,a4     ;VDP data port
        lea    (VDPSetupArray,pc),a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts 
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
update_palette:
		cmpi.b #$00, palette_number
		 beq return
		 
		cmpi.b #$01, palette_number
		 beq ld_palette_1
		 
		cmpi.b #$02, palette_number
		 beq ld_palette_2
		 
		cmpi.b #$03, palette_number
		 beq ld_palette_3			 
		rts 
	
ld_palette_1:	
		move.l #$c0060000,(a3)
		move.w #$02ac,(a4) ;amber
		rts
ld_palette_2:
		move.l #$c0060000,(a3)	
		move.w #$0eee,(a4) ;white
		rts
ld_palette_3:		
		move.l #$c0060000,(a3)
		move.w #$06e6,(a4) ;green
		rts	

update_art:	
		cmpi.b #$01,artnum
       beq ld_art_aperture  ;artnum1
	   cmpi.b #$02,artnum
       beq ld_art_explosion ;artnum2
	   cmpi.b #$03,artnum
       beq ld_art_unknown1  ;artnum3
	   cmpi.b #$04,artnum
       beq ld_art_cake      ;artnum4
	   cmpi.b #$05,artnum
       beq ld_art_mesa      ;artnum5
	   cmpi.b #$06,artnum
       beq ld_art_atom      ;artnum6
	   cmpi.b #$07,artnum
       beq ld_art_fire      ;artnum7
	   cmpi.b #$08,artnum
       beq ld_art_heart     ;artnum8
	   cmpi.b #$09,artnum
       beq ld_art_nuke      ;artnum9
	   cmpi.b #$0a,artnum
       beq ld_art_check     ;artnumA
       bra ld_art_empty

ld_art_aperture:
		lea (ascii_aperture,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_explosion:
		lea (ascii_explosion,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts		
ld_art_unknown1:
		lea (ascii_unknown1,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_cake:
		lea (ascii_cake,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_mesa:
		lea (ascii_mesa,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_atom:
		lea (ascii_atom,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art
		rts	
ld_art_fire:
		lea (ascii_fire,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_heart:
		lea (ascii_heart,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_nuke:
		lea (ascii_nuke,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts
ld_art_check:
		lea (ascii_check,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts	
ld_art_empty:
		lea (ascii_clear,pc),a5
		move.w #0019,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art		
		rts		
		
write_art:
		move.w #$0027,d4
artloop:
		move.b (a5)+,d5	
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,artloop
		move.w #$0017,d4
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		dbf d6, write_art
		rts		
	   
returnint:
	   rte
return:
	   rts	   
   
		
		
 include "data.asm"		
		
		
		
		
		
		
		
		
		
		
		