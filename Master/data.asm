VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8210	;field A    
	dc.w $8300	;$833e	
	dc.w $8405	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8Aff		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D3c		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
music:
	incbin "sound.pcm"
		
font:	
	incbin "ascii.bin"
	;l = too early
	;e = too late
	
line1:	
 dc.b "Forms FORM-29827281-12: "
line2: 
 dc.b "Test Assessment Report  "
line3:
 dc.b "This was a triumph. " 
line4:
 dc.b "I'm making a note here: "
line5:
 dc.b "HUGE SUCCESS. "
line6:
 dc.b "It's hard to overstate  "
line7:
 dc.b "my satisfaction.  "
line8:
 dc.b "Aperture Science  " ;ASCII aperture
line9:
 dc.b "We do what we must  " 
line10: 
 dc.b "because we can. " 
line11: 
 dc.b "For the good of all of us.  " 
line12: 
 dc.b "Except the ones who are dead. " ;ASCII nuke
 
line13: 
 dc.b "But there's no sense crying " ;ASCII aperture
line14: 
 dc.b "over every mistake. " 
line15: 
 dc.b "You just keep on trying "    ;;
line16:
 dc.b "till you run out of cake. "  ;l
line17: 
 dc.b "And the Science gets done.  " ;ASCII nuke
line18: 
 dc.b "And you make a neat gun.  " 
line19: 
 dc.b "For the people who are  " ;ASCII aperture
line20:
 dc.b "still alive.  " 
 
line21:
 dc.b "Forms FORM-55551-5: "
line22:
 dc.b "Personnel File Addendum:  "
line23:
 dc.b "Dear <<Subject Name Here>>, "
line24:
 dc.b "I'm not even angry. "
line25:
 dc.b "I'm being so sincere right now. "
line26:
 dc.b "Even though you broke my heart. ";ASCII heart
line27:
 dc.b "And killed me.  " ;l
line28:
 dc.b "And tore me to pieces.  ";ASCII explosion
line29:
 dc.b "And threw every piece into a fire.";ASCII fire
line30:
 dc.b "As they burned it hurt because  "
line31:
 dc.b "I was so happy for you! ";l ;ASCII check
 
line32:
 dc.b "Now these points of data  "
line33:
 dc.b "make a beautiful line.  "
line34:
 dc.b "And we're out of beta.  "
line35:
 dc.b "We're releasing on time.  ";l
line36:
 dc.b "So I'm GLaD. I got burned.  ";ASCII explosion
line37:
 dc.b "Think of all the things we learned  ";ASCII atom
line38:
 dc.b "for the people who are  " ;ASCII aperture
line39:
 dc.b "still alive.  " 
 
line40:
 dc.b "Forms FORM-55551-6: "
line41:
 dc.b "Personnel File Addendum Addendum: " 
line42:
 dc.b "One last thing: " 
line43:
 dc.b "Go ahead and leave me.  " 
line44:
 dc.b "I think I prefer to stay inside.  " 
line45:
 dc.b "Maybe you'll find someone else  " 
line46:
 dc.b "to help you.  " 
line47:
 dc.b "Maybe Black Mesa... " ;ASCII mesa
line48:
 dc.b "THAT WAS A JOKE. HA HA. FAT CHANCE. " 
line49:
 dc.b "Anyway, this cake is great. " ;ASCII cake
line50:
 dc.b "It's so delicious and moist.  " 
 
line51:
 dc.b "Look at me still talking  ";ASCII unknown (glados?)
line52:
 dc.b "when there's Science to do. " ;ASCII nuke
line53:
 dc.b "When I look out there,  " ;ASCII aperture
line54:
 dc.b "it makes me GLaD I'm not you. "
line55:
 dc.b "I've experiments to run.  ";ASCII atom
line56:
 dc.b "There is research to be done. ";ASCII explosion
line57:
 dc.b "On the people who are ";ASCII aperture
line58:
 dc.b "still alive.  "
 
line59:
 dc.b "PS: And believe me I am "
line60:
 dc.b "still alive.  "
line61:
 dc.b "PPS: I'm doing Science and I'm  "
line62:
 dc.b "still alive.  "
line63:
 dc.b "PPPS: I feel FANTASTIC and I'm  "
line64:
 dc.b "still alive.  "
line65:
 dc.b "FINAL THOUGHT:  "
line66:
 dc.b "While you're dying I'll be  "
line67:
 dc.b "still alive.  "
line68:
 dc.b "FINAL THOUGHT PS: "
line69:
 dc.b "And when you're dead I will be  "
 ;dc.b "And when you're dead I'll be  "
line70:
 dc.b "still alive.  " 
line71:
 dc.b "STILL ALIVE " 
line72:
 dc.b "Still alive.  " 
 
title_splash: 
 dc.b "                                        "
 dc.b "   'Still Alive' Genesis demo program   "
 dc.b "                                        "
 dc.b " Programming by ComradeOj               "
 dc.b " Testing by ComradeOj                   " 
 dc.b " Music and original concept by Valve    "
 dc.b "                                        "
 dc.b " Instructions:                          "
 dc.b " Run this program on a flash cart.      "
 dc.b " Run the slave program on a flash cart, "
 dc.b " or burn the SEGA CD version onto a CD. "
 dc.b " Link the second controller port of the "
 dc.b " Genesis running this program to the    "
 dc.b " first controller port of the Genesis   "
 dc.b " running the slave program. Connect all "
 dc.b " corresponding pins except the +5V pin. "
 dc.b " Do this at your own risk.              " 
 dc.b " I am not responsible for any damage    "
 dc.b " done to your console.                  "
 dc.b "                                        "   
 dc.b "           Free shoutouts to:           "   
 dc.b " SEGA-16.com & reddit.com/r/retrogaming "
 dc.b "                                        " 
 dc.b " Press A to set amber color (original)  "
 dc.b " Press B to set green color             "
 dc.b " Press C to set white color             "
 dc.b "          Press START to begin          "
 dc.b "                                        "
 
end_splash:
 ; dc.b "  ______  ______  __  __      __        " 
 ; dc.b " /\  ___\/\__  _\/\ \/\ \    /\ \       " 
 ; dc.b " \ \___  \/_/\ \/\ \ \ \ \___\ \ \____  " 
 ; dc.b "  \/\_____\ \ \_\ \ \_\ \_____\ \_____\ " 
 ; dc.b "   \/_____/  \/_/  \/_/\/_____/\/_____/ " 
 ; dc.b "   ______  __      __  __   ________    " 
 ; dc.b "  /\  __ \/\ \    /\ \/\ \ / /\  ___\   " 
 ; dc.b "  \ \  __ \ \ \___\ \ \ \ \`/\ \  __\   " 
 ; dc.b "   \ \_\ \_\ \_____\ \_\ \__| \ \_____\ " 
 ; dc.b "    \/_/\/_/\/_____/\/_/\/_/   \/_____/ "   
 
 dc.b "         _____   _     _   _   _        "
 dc.b "        / ____| | |   (_) | | | |       "
 dc.b "       | (___   | |_   _  | | | |       "
 dc.b "        \___ \  | __| | | | | | |       "
 dc.b "        ____) | | |_  | | | | | |       "
 dc.b "       |_____/   \__| |_| |_| |_|       "	
 dc.b "               _   _                    "
 dc.b "       /\     | | (_)                   "
 dc.b "      /  \    | |  _  __   __   ___     "
 dc.b "     / /\ \   | | | | \ \ / /  / _ \    "
 dc.b "    / ____ \  | | | |  \ - /  |  __/    "
 dc.b "   /_/    \_\ |_| |_|   \_/    \___|    "
 dc.b "          ___________________           "  
 dc.b "         /         ------    \          "  
 dc.b "        |---      /      \    |         "
 dc.b "        |---     /        \   |         "
 dc.b "        |---    | ======== |  |         "
 dc.b "        |---    | GENESIS  |  |         "
 dc.b "        |---    |          |  |         "
 dc.b "        |---     \ 16-BIT /   |         "
 dc.b "        |         \______/    |         "
 dc.b "        | ||      \------/    |         "
 dc.b "        | ## |=               |         "
 dc.b "        | ||                  |         " 
 dc.b "        | || ==               |         "
 dc.b "        |\___________________/|         "  
 dc.b "         \--o---------##--##-/          "  
 dc.b "        OCTOBER 29, 1988 - NEVER        " 

background:
 dc.b " -------------------------------------  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " |                                   |  "
 dc.b " -------------------------------------  "
 
empty_page: 
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "
 dc.b "                                        "

 