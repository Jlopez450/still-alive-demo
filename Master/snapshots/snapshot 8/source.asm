    include "ramdat.asm"
		      dc.l $FFFE00,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA MEGASIS    "
              dc.b "(C)2015 James L."
              dc.b "PROGRAM NAME HERE                               "
              dc.b "PROGRAM NAME HERE                               "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
        bsr setup_vdp
        move.w  #$100,($A11100)	; send a Z80 a bus request
        move.w  #$100,($A11200)	; reset the Z80    
        move.b #$2b,($A04000)         ;DAC enable register             
        move.b #$80,($A04001)         ;enable DAC             
        move.b #$2a,($A04000)         ;DAC register	
		lea (music),a2
		bsr clear_vram
		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$0800,d4
		bsr vram_loop
		bsr draw_bg
		bsr set_scroll
		
		move.l #$c0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$02ac,(a4)
		move.l #$00000000,d2
		;bra ending
        move.w #$2300, sr       ;enable ints
		bsr writeline1
wait1:		
		cmpi.b #$ff,continue
		bne wait1
		
		bsr writeline2
wait2:		
		cmpi.b #$ff,continue
		bne wait2		
		add.b #$01,line_number	;critical
loop:
		bsr playsound
		bsr check_text
		bra loop
		
check_text:		
		lea (text_table),a0
		move.l #$00000000,d0
		move.b line_number,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

text_table:			
 dc.w checkline1	
 dc.w checkline2	
 dc.w checkline3	
 dc.w checkline4	
 dc.w checkline5	
 dc.w checkline6	
 dc.w checkline7	
 dc.w checkline8	
 dc.w checkline9	
 dc.w checkline10	
 dc.w checkline11	
 dc.w checkline12	
 dc.w checkline13
 dc.w checkline14
 dc.w checkline15
 dc.w checkline16
 dc.w checkline17
 dc.w checkline18
 dc.w checkline19
 dc.w checkline20
 dc.w checkline21
 dc.w checkline22
 dc.w checkline23
 dc.w checkline24
 dc.w checkline25
 dc.w checkline26
 dc.w checkline27
 dc.w checkline28
 dc.w checkline29
 dc.w checkline30
 dc.w checkline31
 dc.w checkline32
 dc.w checkline33
 dc.w checkline34
 dc.w checkline35
 dc.w checkline36
 dc.w checkline37
 dc.w checkline38
 dc.w checkline39
 dc.w checkline40
 dc.w checkline41
 dc.w checkline42
 dc.w checkline43
 dc.w checkline44
 dc.w checkline45
 dc.w checkline46
 dc.w checkline47
 dc.w checkline48
 dc.w checkline49
 dc.w checkline50
 dc.w checkline51
 dc.w checkline52
 dc.w checkline53
 dc.w checkline54
 dc.w checkline55
 dc.w checkline56
 dc.w checkline57
 dc.w checkline58
 dc.w checkline59
 dc.w checkline60
 dc.w checkline61
 dc.w checkline62
 dc.w checkline63
 dc.w checkline64
 dc.w checkline65
 dc.w checkline66
 dc.w checkline67
 dc.w checkline68
 dc.w checkline69
 dc.w checkline70
 dc.w checkline71
 dc.w checkline72
 dc.w checkline73
 dc.w checkline74
 
 
	include "linecheck.asm"

write_line:
		move.w #$0027,d4
textloop:
		cmpi.b #$04, text_timer
		 blt return
		move.b #$00, text_timer
		cmpi.w #$0000,d4
		 beq end_text
		move.b (a5)+,d5	
		andi.w #$00ff,d5
        move.w d5,(a4)
		sub.w #$0001,d4
		rts
end_text:
		move.b #$ff,continue	
		rts
		
VBlank:
		add.b #$01,text_timer
	    bsr textloop
        rte	

	include "linewrite.asm"
			
playsound:      
        bsr delay
		bsr test2612	
		move.b (a2)+,$A04001      ;write to DAC	
		add.l #$00000001,d2		  ;PCM counter
		rts
delay:
        nop
        nop
        nop
        nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
        ; nop
		rts
test2612:
        move.b $A04001,d0
        btst #$80,d0
		beq delay 
		rts			
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts
		
set_scroll:
		move.l #$0000F000,d0
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0013,(a4)		
		move.w #$0004,(a4)		
	    move.l #$40000010,(a3)   ;write to vsram    
		move.w #$01f8,(a4)
		move.w #$0000,(a4)	
		rts
		
clear_page:		       
		move.l #$00004000,d0	;write location
		bsr calc_vram
		move.l d0,(a3)
		move.w #$0800,d7
clear_page_loop:             
        move.w  #$0000,(a4)
        dbf d7,clear_page_loop
        rts			

calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
draw_bg:
		lea (background),a5
		move.w #0028,d6   ;height
		move.l #$0000A000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art
		rts			
		
ending:
		move.w #$2700,sr
		lea (end_splash),a5
		move.w #0028,d6   ;height
		move.l #$00004000,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr write_art			
		
end_hang:		
		bra end_hang
		
		
write_art:
		move.w #$0027,d4
artloop:
		move.b (a5)+,d5	
		andi.w #$00ff,d5
        move.w d5,(a4)	
		dbf d4,artloop
		move.w #$0017,d4
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		dbf d6, write_art
		rts		
	
ErrorTrap:        
        bra ErrorTrap

HBlank:
		rte
returnint:
	   rte
return:
	   rts
	include "data.asm"

ROM_End:
              
              